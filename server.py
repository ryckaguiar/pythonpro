#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import time

def main():

    #print("{:02x}".format(b'\x00\x02o\xae\x93:'))
    r = list(map("{:02x}".format, b'\x00\x02o\xae\x93:'))
    print(':'.join(r).upper())


def init_server(sock):
    host = '127.0.0.1'
    port = 8000
    connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    connection.bind((host, port))
    print('Server started')
    while 1:
        data, addr = sock.recv(1024)
        print(str(data) + str(addr))

    connection.close()

if __name__ == '__main__':
    main()


