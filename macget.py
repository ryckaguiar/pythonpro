#!/usr/bin/env python3
import socket
import struct
# from threading import Thread
import time


def main():

    s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    x = 0
    while True:
        raw_data, addr = s.recvfrom(65565)
        dest_mac, src_mac, proto, data = ethernet_frame(raw_data)
        #interger = struct.unpack('h', raw_data[12:14])
        print(raw_data[12:14])
        print('\nHost:')
        print(addr)
        print('\nEthernet Frame')
        print('Destination: {}, Source: {}, Protocol: {}'.format(dest_mac, src_mac, proto))

def ethernet_frame(data):
    dest_mac, src_mac, proto = struct.unpack('! 6s 6s H', data[:14])
    return get_mac_addr(dest_mac), get_mac_addr(src_mac), socket.htons(proto), data[14:]


def get_mac_addr(bytes_addr):
    bytes_str = map('{:02x}'.format, bytes_addr)
    return ':'.join(bytes_str).upper()


if __name__ == '__main__':
    main()
