import testes
import time
import os
import json


caminho = '/home/ricardo/'
file = '/home/ricardo/arq_lidos.txt'
arquivos = []
existe = []

if not os.path.exists(file):
    print("Arquivo não Existe, mas estamos criando para Você!")
    fileW = open(file, 'w')
    print("Aguarde ...")
    time.sleep(2)
    fileR = open(file, 'r')
elif os.path.exists(file):
    print("Pronto! o Arquivo está no diretório e podemos iniciar.")
    print("Aguarde ...")
    time.sleep(2)
    fileR = open(file, 'r')
else:
    print("Não foi possível criar este arquivo, verifique suas permissões de usuário!")

if fileR.read() != '':
    fileR = open(file, 'r')
    data_json = json.load(fileR)
    arquivos = [propi for propi in data_json['Proprietarios']]
    for prop in arquivos:
        existe.append(prop['nome'])
    fileR.close()
fileR.close()
xi = 0

for extensao in os.listdir(caminho):
    if extensao.lower().endswith('.txt'):
        if(existe == [] or not extensao in existe):
            print('i am entry in loop 1')
            arquivos.append({
                'id': xi,
                'nome': extensao
            }
        )
        xi += 1


fileW = open(file, 'w')
fileW.write(json.dumps({'Proprietarios': arquivos}))
fileW.close()

