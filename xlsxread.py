#!/usr/bin/python
# -*- coding: UTF-8 -*-

from openpyxl import load_workbook
import os
import time
import json


def main():
    lotes = memorial_descritivo()
    directory = '/home/ricardo/Arquivos_HenriquetaPrates/'
    users = []
    extension_a = []
    extension_b = []
    filer = open(file(), 'r')
    if filer.read() != '':
        filer = open(file(), 'r')
        data_users_json = json.load(filer)
        user_list = [user for user in data_users_json]
        for e in user_list:
            extension_b.append(e['extension'])
        filer.close()

    for extension in os.listdir(directory):
        if extension.lower().endswith('.xlsx') or extension.lower().endswith('.xls'):
            extension_a.append(extension)
            if not extension_a in extension_b or extension_b == []:
                users.append(users_generate(directory + extension, extension, lotes))

    if set(extension_a) == set(extension_b):
        print("Nenhum arquivo novo encontrado!")
    else:
        file_w = open(file(), 'w')
        file_w.write(json.dumps(users))
        file_w.close()
        print('Base de dados gerada com sucesso!')


def file():
    file_ = '/home/ricardo/database_HP.txt'
    if not os.path.exists(file_):
        print("Arquivo não Existe, mas estamos criando para Você!")
        file_w = open(file_, 'w')
        file_w.close()
        print("Aguarde ...")
        time.sleep(2)
        return file_
    elif os.path.exists(file_):
        print("Pronto! o Arquivo está no diretório e podemos iniciar.")
        print("Aguarde ...")
        time.sleep(2)
        return file_
    else:
        print("Não foi possível criar este arquivo, verifique suas permissões de usuário!")


def users_generate(path, extension, lotes):
    lw = load_workbook(filename=path)
    sheet = lw.active

    listapessoas = []
    list_peoples = (pessoa for pessoa in membros_familia(path))
    for p in list_peoples:
        if p['parentesco'] != 'None':
            listapessoas.append(p)
    lote = {}
    lotes_list = (lote for lote in lotes)

    for l in lotes_list:
        if (l['lote'] == str(sheet['B13'].value)):
            lote = l

    b9 = str(sheet['B9'].value).split()
    b9 = str().join(b9)
    if b9.lower() == '[x]Masculino[]Feminino'.lower():
        b9 = 'Masculino'
    elif b9.lower() == '[]Masculino[X]Feminino'.lower():
        b9 = 'Feminino'
    else:
        b9 = ''
    users = dict(extension=extension, codigo=sheet['J4'].value, nome=str(sheet['B5'].value), cpf=str(sheet['B6'].value),
                 nis=str(sheet['H6'].value), rg=str(sheet['B7'].value), exp=str(sheet['H7'].value),
                 tituloEleitoral=str(sheet['C8'].value), uf=str(sheet['K7'].value), zona=str(sheet['H8'].value),
                 secao=str(sheet['K8'].value), sexo=b9, dataNascimento=str(sheet['I9'].value),
                 lote=str(sheet['B13'].value), quadra=str(sheet['E13'].value), frente=lote['frente'],
                 fundo=lote['fundo'], lateralesquerda=lote['lateralesquerda'], lateraldireita=lote['lateraldireita'],
                 pessoasfamilia=listapessoas, cpfconjuge=str(sheet['E24'].value), rgconjuge=str(sheet['I24'].value), estadocivil=str(sheet['H16'].value)
                 )

    return users


def membros_familia(path):
    lw = load_workbook(filename=path)

    sheet = lw.active

    pessoas_familia = []

    for i in range(16, 25):
        cellA = sheet['A'.__add__(str(i))]
        cellF = sheet['F'.__add__(str(i))]
        cellG = sheet['G'.__add__(str(i))]
        cellH = sheet['H'.__add__(str(i))]
        cellI = sheet['I'.__add__(str(i))]
        cellJ = sheet['J'.__add__(str(i))]
        cellK = sheet['K'.__add__(str(i))]
        pessoa = dict(nome=str(cellA.value), parentesco=str(cellF.value), idade=str(cellG.value),
                      estadocivil=str(cellH.value), escolaridade=str(cellI.value), profissao=str(cellJ.value),
                      renda=str(cellK.value))

        pessoas_familia.append(pessoa)

    return pessoas_familia


def memorial_descritivo():
    directory = '/home/ricardo/Arquivos_HenriquetaPrates/MemorialDescritivo/'

    file = os.listdir(directory)

    filer = str(directory).__add__(file[0])

    lw = load_workbook(filename=filer)

    memorial_sheet = lw.active

    lotes = []

    for i in range(1, 100):
        cellA = 'A'.__add__(str(i))
        cellB = 'B'.__add__(str(i))
        cellD = 'D'.__add__(str(i))
        cellF = 'F'.__add__(str(i))
        cellH = 'H'.__add__(str(i))
        cellJ = 'J'.__add__(str(i))
        lote = {
            'lote': str(memorial_sheet[cellA].value),
            'frente': str(memorial_sheet[cellB].value),
            'fundo': str(memorial_sheet[cellD].value),
            'lateraldireita': str(memorial_sheet[cellF].value),
            'lateralesquerda': str(memorial_sheet[cellH].value),
            'area': str(memorial_sheet[cellJ].value)
        }
        lotes.append(lote)

    return lotes


if __name__ == "__main__":
    main()
