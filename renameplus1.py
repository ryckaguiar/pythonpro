#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os


def getfilename():
    directory = "/media/ricardo/GERAL/Cartorio_PDFS/Matriculas_RA_QR/"

    fileNames = []

    for filename in os.listdir(directory):
        fileNames.append(filename)

    files_sorted = sorted(fileNames)
    init = 106916
    for file in files_sorted:
        os.rename(directory + file, directory + file + ".pdf")
        init += 1

    print("Finished!")
if __name__ == '__main__':
    getfilename()