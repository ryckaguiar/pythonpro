#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import threading
from threading import Thread
import time


class AsyncWrite(threading.Thread):
    def __init__(self, text, out):
        threading.Thread.__init__(self)
        self.text = text
        self.out = out

    def run(self):
        f = open(self.out, "a")
        f.write(self.text + "\n")
        f.close()
        time.sleep(5)
        print('Finished background write file to' + self.out)


def main():
    message = input("Enter a string to store: ")
    background = AsyncWrite(message, 'out.txt')
    background.start()
    print('program can continue run in a background')
    background.join()
    print('wait until thread was completed')
    main1()

def get_time(name, delay, repeat):
    print("Time: " + name + " started")
    while repeat > 0:
        time.sleep(delay)
        print(name + ': ' + str(time.ctime(time.time())))
        repeat -= 1
    print("Timer: " + name + " completed")


def main1():
    t1 = Thread(target=get_time, args=('Timer1', 1, 5))
    t2 = Thread(target=get_time, args=('Timer2', 2, 5))
    t1.start()
    t2.start()


if __name__ == '__main__':
    main()
